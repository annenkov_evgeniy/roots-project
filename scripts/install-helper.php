<?php

require_once( __DIR__ . '/../vendor/autoload.php' );

$root_dir = __DIR__ . '/..';
$dotenv   = new Dotenv\Dotenv( $root_dir );

/**
 * Use Dotenv to set required environment variables and load .env file in root
 */
if ( file_exists( $root_dir . '/.env' ) ) {
	$dotenv->load();
}
